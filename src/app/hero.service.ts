import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

export const HEROES: Hero[] = [
  {id: 11, name: 'Mr. Nice'},
  {id: 12, name: 'Narco'},
  {id: 13, name: 'Bombasto'},
  {id: 14, name: 'Celeritas'},
  {id: 15, name: 'Magneta'},
  {id: 16, name: 'RubberMan'},
  {id: 17, name: 'Dynama'},
  {id: 18, name: 'Dr IQ'},
  {id: 19, name: 'Magma'},
  {id: 20, name: 'Tornado'}
];

@Injectable()
export class HeroService {
  public getHeroes(): Observable<Hero[]> {
    return new Observable<Hero[]>(subscriber => {
      console.log('wait for heroes..');
      setTimeout(() => {
        console.log('heroes arrived!');
        subscriber.next(HEROES);
      }, 500);
    });

    // return  new Observable(subscriber => {
    //   console.log('wait for heroes..');
    //   setTimeout(1000, () => {
    //     console.log('heroes arrived!');
    //     subscriber.next(HEROES);
    //   });
    // });
  }

  public getHero(id: number): Observable<Hero> {
    return this.getHeroes()
      .map(heroes => heroes.find(hero => hero.id === id));
  }
}
