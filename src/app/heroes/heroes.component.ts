import { Component, Input, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { Router } from '@angular/router';

@Component({
  selector: 'my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: [ './heroes.component.css' ]
})
export class HeroesComponent implements OnInit {
  public title = 'Tour of Heroes';

  public selectedHero: Hero;
  public heroes: Hero[];
  private lastError: any;
  private finished: boolean;

  constructor(
    private heroService: HeroService,
    private router: Router,
  ) {
  }

  public ngOnInit() {
    this.getHeroes();
  }

  public getHeroes(): void {
    this.heroService.getHeroes().subscribe(
      value => this.heroes = value,
      error => this.lastError = error,
      () => this.finished = true
    );
  }

  public onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  public gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

}
